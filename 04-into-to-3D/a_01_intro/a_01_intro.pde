void setup() {
    size(500,500,P3D);
    rectMode(CENTER);
}
float z = 0,y = 0;
int nx = 240;
float scale = 200;
float numframes = 240;
void draw() {
    background(0);
    translate(width / 2,height / 2);
    noStroke();
    pushMatrix();
    y = TWO_PI * frameCount / numframes;
    translate(0,0,0);
    rotateZ(y);
    // rotateX(z);
    for (int i = -nx; i <= nx; ++i) {
        pushMatrix();
        translate(i * scale / nx,0,0);
        rotateX(i * HALF_PI / nx - TWO_PI * frameCount / numframes);
        rect(0, 0, scale / nx,scale);
        popMatrix();
    }
    popMatrix();    
    
    if (frameCount <=  numframes) {
        // saveFrame("./frames/fr###.png");
    }
}
