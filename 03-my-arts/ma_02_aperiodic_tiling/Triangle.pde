class Triangle{
    PVector a,b,c;
    int colorid;
    float goldenRatio = (1 + sqrt(5)) / 2;
    color col;
    public Triangle(PVector a, PVector b,PVector c, int colorid) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.colorid = colorid;
        if(colorid == 0)
           this.col = 220;
        else{
           this.col = 0;    
        }
    }
        
    void draw() {
        fill(this.col);
        noStroke();
        beginShape();
        vertex(a.x, a.y);
        vertex(b.x, b.y);
        vertex(c.x, c.y);
        endShape(CLOSE);
    }        
    ArrayList<Triangle>  getNext() {
        ArrayList<Triangle> triangles = new ArrayList();
        if (this.colorid == 0) {
            PVector p = PVector.add(a,PVector.div(PVector.sub(b,a),goldenRatio));
            triangles.add(new Triangle(c, p, b,0));
            triangles.add(new Triangle(p, c, a,1));
        }
        else{
            PVector q = PVector.add(b,PVector.div(PVector.sub(a,b),goldenRatio));
            PVector r = PVector.add(b,PVector.div(PVector.sub(c,b),goldenRatio));
            triangles.add(new Triangle(r, c, a,1));
            triangles.add(new Triangle(q, r, b,1));
            triangles.add(new Triangle(r, q, a,0));
        }
        return triangles;
    }
}