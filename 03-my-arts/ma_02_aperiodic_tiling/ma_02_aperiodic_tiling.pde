PVector a,b,c;
int numgenerations = 8;
int n = 1;
ArrayList<Triangle> tgen1,tgen2;
ArrayList<Triangle> tgen1_,tgen2_;
float deg = PI / 180;
void setup() {
    fullScreen();
    float r = height / 2;
    b = new PVector(r,0);
    c = new PVector(r * cos(TWO_PI/10),r * sin(TWO_PI/10));
    Triangle t  = new Triangle(new PVector(0,0),b,c,0);
    tgen1 = new ArrayList();
    tgen2 = new ArrayList();

    tgen1.add(t);
    tgen2.add(t);

}

void draw() {
    translate(width / 2,height / 2);
    for (int i = 0; i < 10; ++i) {
        pushMatrix();
        rotate(i * TWO_PI);
        scale(pow(-1,i)*1, 1);
        for (Triangle triangle : tgen1) {
            triangle.draw();
        }
        popMatrix();
    }
    delay(1000);
    if (frameCount < numgenerations){
        saveFrame("./frames/fr###.png");
        nextGen();
    }
    else
        stop();
}

void nextGen() {
    for (Triangle tr1 : tgen1) {
        ArrayList<Triangle> temp  =  tr1.getNext();
        for (Triangle t : temp) {
            tgen2.add(t);
        }
    }
    tgen1.clear();
    for (Triangle o : tgen2) {
        tgen1.add(o);
    }
}