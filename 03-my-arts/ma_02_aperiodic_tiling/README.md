# Aperiodic tiling
This Aperiodic tiling genaration method is followd from https://preshing.com/20110831/penrose-tiling-explained/. The basic steps in generating these tiles is as follows

1.  We start with the two types of isosceles triangles as shwon.<br>![both triangles](./tutorial-files/red-blue-triangle.png)

2. As you can see the base of these triangles have no line this allow tow triangles to connect to each other and form rhombus-shaped tiles that are visible in the final Penrose tiling.<br>![connection](./tutorial-files/connection.png)

3. Subdivide the red triangle into two 1 red and 1 blue triangle.<br>![red dubdivide](./tutorial-files/red-triangle-subdivision.png)<br>
Blue triangle can be subdivided into three 2 blue and 1 red triangles<br>![blue dubdivide](./tutorial-files/blue-triangle-subdivision.png)

4. The new vertices P,Q and R all follows golden ratio. These vertices can be obtained by<br>
```
GR = (1+sqrt(5))/2 # Golden ratio
P = A+(B-A)/GR
Q = B + (A - B) / GR
R = B + (C - B) / GR
```

5. Now you can get idea where we are proceeding<br>
![sequence](./tutorial-files/triangle-sequence.png)

6. So to get aesthetically appealing images you can do something like<br>
![wheel](./tutorial-files/wheel-sequence.png)