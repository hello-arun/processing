int numFrames = 120;
float t=0;
Moon[] moons;
color[] palette={#00000f,#ffffff,#264653,#2a9d8f};//color(value1, value2, value3);
void setup() {
    size(500, 500);
    ellipseMode(CENTER);  
    imageMode(CENTER);  
    noiseSeed(1);
    moons= new Moon[3];
    float fact=0.6;
    moons[0] = new Moon(width/2,height/2,width,0.9*width,width*0.10,1,1);  
    moons[1] = new Moon(width/2,height/2+35,(int)(fact*width),fact*0.9*width,fact*width*0.10,-1,2);  
    moons[2] = new Moon(width/2,height/2+60,(int)(fact*fact*width),fact*fact*0.9*width,fact*fact*width*0.10,1,1);  
    


}

void draw() {
  smooth();
  t=1.0*frameCount/numFrames;
    background(palette[0]);
    for (Moon moon : moons) {
     moon.show(t); 
    }
//     float t = 1.0*frameCount/numFrames;
//     float r = width / 2;
//     float x = width / 2,y = height / 2;
//     float r2;
//     noFill();
//     noStroke();
//     background(palette[0]);
    
//     float fact = 0.8;//+0.5*abs(sin(0.5*t));
//     for (int i = 1; i < 15; ++i) {
//         fill(palette[(i) % 4]);
//         ///stroke(c[(i-1)%4]);
//         ellipse(x, y, 2 * r, 2 * r);
//         r2 = r * fact;
//         x += (r - r2 + 10) * cos(t + PI / 2 * (i % 3));
//         y += (r - r2 + 10) * sin(t + PI / 2 * (i % 6));
//         r *=  fact;
// }
  if(frameCount<=numFrames){
     saveFrame("./frames/fr###.png");
  }  
}
        
      




      /**
 * Draw a Mask example Pt.2
 * 2017-08-22 Processing 3.3.5
 *
PGraphics sourceImage;
PGraphics maskImage;
void setup() {
  size(512, 512);

  // create source
  sourceImage = createGraphics(512,512);
  sourceImage.smooth();
  sourceImage.beginDraw();
  sourceImage.fill(255,0,0);
  sourceImage.noStroke();
  sourceImage.circle(width/2, height/2, 512);
  // sourceImage.rect(100, 100, 312,312);

  sourceImage.endDraw();

  // create mask
  maskImage = createGraphics(512,512);
  maskImage.beginDraw();
  maskImage.background(255);
  maskImage.fill(0);
  maskImage.smooth();
  maskImage.noStroke();
  maskImage.circle(width/2,height/2,100);
  maskImage.endDraw();

  // // apply mask
  sourceImage.mask(maskImage);
}
void draw() {
  background(255);
  // show masked source
  image(sourceImage, 10, 0);
}*/