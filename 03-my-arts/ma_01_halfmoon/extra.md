To make samsung phone wallpaper use

Windows
``` 
magick anim.gif -background black -gravity south -extent 500x800 temp.gif
magick temp.gif -bordercolor black  -border 0x300 anim_android.gif
rm temp.gif
```

Linux
``` 
convert anim.gif -background black -gravity south -extent 500x800 temp.gif
convert temp.gif -bordercolor black  -border 0x300 anim_android.gif
rm temp.gif
```