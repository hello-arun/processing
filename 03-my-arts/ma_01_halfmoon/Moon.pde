class Moon{
    float cx,cy,r1,r2,shift;
    PGraphics source,mask;
    float side = 1,speed=1;

    Moon(float cx,float cy,int r1,float r2,float shift,float side,float speed) {
        this.cx = cx;
        this.cy = cy;
        this.r1 = r1;
        this.r2 = r2;
        this.shift = shift;
        this.side = side;
        this.speed= speed;
        source = createGraphics(r1,r1);
        mask   = createGraphics(r1,r1);
        source.beginDraw();
        source.noStroke();
        source.background(palette[1]);
        source.fill(palette[0]);
        // source.fill(palette[0]);
        source.endDraw();

        mask.beginDraw();
        mask.noStroke();

        mask.background(0);
        mask.fill(255);
        mask.ellipse(r1/2,r1/2,r1,r1);
        mask.fill(0);

        mask.ellipse(r1/2,r1/2 + shift,r2,r2);
        // mask.ellipse(r1/2,r1/2,r1,r1);

        mask.endDraw();

        source.mask(mask);
        source.smooth();
    }
    void show(float t) {
 pushMatrix();
 translate(cx,cy);

//  rot+=0.05*pow(noise(cy,2*cos(2*PI*t),2*sin(2*PI*t)),0.3);
 rotate(2*PI*t*side*speed);
 image(source,0,0);
 popMatrix();
    }
}