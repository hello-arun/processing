# Aperiodic tiling

This is in continuation of previous work in [ma_02_aperiodic_tiling](../ma_02_aperiodic_tiling). It is advised to follow that tuorial to get this.

## Final product

![video](./anim.mp4)

# How to generate this anim.mp4

```
# use ./duplicate.py to duplicate last frame few more times
ffmpeg.exe -framerate 50 -i ./frames/fr%03d.png -profile:v main -pix_fmt yuv420p  1.mp4
ffmpeg -i ./1.mp4 -vf reverse 2_.mp4
ffmpeg -i ./2_.mp4 -filter:v  "setpts=0.9*PTS" 2.mp4
# create file list.txt with UTF-8 encoding and write
# file 1.mp4
# file 2.mp4
ffmpeg -f concat -i list.txt -c copy anim.mp4
```
