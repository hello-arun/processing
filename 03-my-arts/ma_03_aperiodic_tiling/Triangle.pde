class Triangle{
    PVector a,b,c;
    int colorid;
    float goldenRatio_INV = 2/(1 + sqrt(5));
    color col;
    public Triangle(PVector a, PVector b,PVector c, int colorid) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.colorid = colorid;
        if(colorid == 0)
           this.col = 220;
        else{
           this.col = 0;    
        }
    }
        
    void draw() {
        fill(this.col);
        noStroke();
        beginShape();
        vertex(a.x, a.y);
        vertex(b.x, b.y);
        vertex(c.x, c.y);
        endShape(CLOSE);
    }        
    ArrayList<Triangle>  getNext(float t) {
        ArrayList<Triangle> triangles = new ArrayList();
        if (this.colorid == 0) {
            PVector p = PVector.add(a,PVector.mult(PVector.sub(b,a),t*goldenRatio_INV));
            triangles.add(new Triangle(c, p, b,0));
            triangles.add(new Triangle(p, c, a,1));
        }
        else{
            PVector q = PVector.add(b,PVector.mult(PVector.sub(a,b),t*goldenRatio_INV));
            PVector r = PVector.add(b,PVector.mult(PVector.sub(c,b),t*goldenRatio_INV));
            triangles.add(new Triangle(r, c, a,1));
            triangles.add(new Triangle(q, r, b,1));
            triangles.add(new Triangle(r, q, a,0));
        }
        return triangles;
    }

    Triangle copy(){
       return new Triangle(this.a, this.b, this.c, this.colorid);
    }
}