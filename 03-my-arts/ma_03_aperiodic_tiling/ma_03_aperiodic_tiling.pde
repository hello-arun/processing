PVector a,b,c;
int n = 1;
ArrayList<Triangle> tgen1,tgen2;
ArrayList<Triangle> tgen1_,tgen2_;
float deg = PI / 180;

int numgenerations = 7;
int framePerGen=50;
int numframes=framePerGen*numgenerations;
float t = 0;
void setup() {
    size(500,500,P2D);
    float r = height / 2;
    b = new PVector(r,0);
    c = new PVector(r * cos(TWO_PI/10),r * sin(TWO_PI/10));
    Triangle triangle  = new Triangle(new PVector(0,0),b,c,0);
    tgen1 = new ArrayList();
    tgen2 = new ArrayList();
    tgen1_ = new ArrayList();
    tgen2_ = new ArrayList();

    tgen1.add(triangle);
    tgen2.add(triangle);
    tgen1_.add(triangle);
    tgen2_.add(triangle);

    smooth(5);
}

void copy(ArrayList<Triangle> a, ArrayList<Triangle> b){
    b.clear();
    for (Triangle triangle : a) {
        b.add(triangle);
    }
}

void draw() {
    background(0);
    translate(width / 2,height / 2);
    rotate(-TWO_PI / 20);

    for (int i = 0; i < 10; ++i) {
        pushMatrix();
        rotate(i * TWO_PI / 10);
        scale(pow(-1,i)*1, 1);
        for (Triangle triangle : tgen2) {
            triangle.draw();
        }
        popMatrix();
    }
    t=1.0*(frameCount%framePerGen)/framePerGen;
    if (frameCount <= numframes){
        saveFrame("./frames/fr###.png");
        nextGen(ease(t,3.0));
        // println(tgen1_.size());
    }
    else
        stop();
    delay(100);
}

void nextGen(float t) {
    if(t==0.0){
        tgen2.clear();
        for (Triangle tr1 : tgen1_) {
            ArrayList<Triangle> temp  =  tr1.getNext(1.0);
            for (Triangle triangle : temp) {
                tgen2.add(triangle);
            }
        }
        copy(tgen2,tgen1_);
        tgen2.clear();
    }
    tgen1.clear();
    tgen2.clear();
    // tgen1=tgen1_;
    copy(tgen1_,tgen1);
    for (Triangle tr1 : tgen1) {
        ArrayList<Triangle> temp  =  tr1.getNext(t);
        for (Triangle triangle : temp) {
            tgen2.add(triangle);
        }
    }
    tgen1.clear();
}  

float ease(float p, float g) {
  if (p < 0.5) 
    return 0.5 * pow(2*p, g);
  else
    return 1 - 0.5 * pow(2*(1 - p), g);
}
void nextGen2(float t) {
    copy(tgen1_,tgen1);
    for (Triangle tr1 : tgen1) {
        ArrayList<Triangle> temp  =  tr1.getNext(t);
        for (Triangle triangle : temp) {
            tgen2.add(triangle);
        }
    }
    tgen1.clear();
    for (Triangle o : tgen2) {
        tgen1.add(o);
    }
}