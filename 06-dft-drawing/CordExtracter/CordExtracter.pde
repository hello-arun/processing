PImage refImage;
ArrayList<int[]> markers;
void setup() {
    size(1024,768);markers = new ArrayList<int[]>();
    refImage = loadImage("./data/arun-sign.jpeg");
    println("width : " + refImage.width);
    println("height : " + refImage.height);
}

void draw() {
    image(refImage, 0, 0, refImage.width, refImage.height);
    strokeWeight(10);
    noFill();
    beginShape();
    for (int[] marker : markers) {
        vertex(marker[0],marker[1]);
    }
    endShape();   
}

void mouseReleased() {
    int[] marker = new int[2];
    marker[0] = mouseX;
    marker[1] = mouseY;
    markers.add(marker);
}

void keyReleased() {
    if (key ==  ' ') {
        JSONArray points = new JSONArray();
        for (int i = 0; i < markers.size(); i++) {
            JSONObject point = new JSONObject();
            point.setInt("x",markers.get(i)[0]);
            point.setInt("y",markers.get(i)[1]);
            points.setJSONObject(i,point);
        }
        saveJSONArray(points, "data/new.json");
    }
    if (key ==  'x') {
        markers.remove(markers.size()-1);
    }
}