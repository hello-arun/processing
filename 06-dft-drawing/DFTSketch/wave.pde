class Phasor{
  float amplitude, frequency, phase;
  
  Phasor(float A, float f, float p){
   amplitude = A;
   frequency = f;
   phase = p;
  } 
  PVector state(float time, float offset){
    return PVector.fromAngle((frequency * time) + phase + offset).mult(amplitude);
  }

String toString(){
  return "Amp: "+this.amplitude+", Freq: "+this.frequency+", Phase: "+this.phase;
}
}


void Sort(Phasor[] Phasors)
{
  int n = Phasors.length;

  for (int i = 0; i < n-1; i++)
  {
    int mindex = i;

    for (int j = i+1; j < n; j++)
    {
      if (Phasors[j].amplitude > Phasors[mindex].amplitude)
        mindex = j;
    }
    swap(Phasors, mindex, i);
  }
}


void swap(Phasor[] Phasors, int i, int j)
{
  Phasor temp = Phasors[i];
  Phasors[i] = Phasors[j];
  Phasors[j] = temp;
}

Phasor[] reduce(Phasor[] phasors,float cutoffAmp,float freqCut){
  ArrayList<Phasor> reducedPhasors = new ArrayList();
  for (Phasor phasor : phasors) {
    if(phasor.amplitude > cutoffAmp && phasor.frequency < freqCut){
      reducedPhasors.add(phasor);
    }
  }
  Phasor[] newPhasors=new Phasor[reducedPhasors.size()];
  for (int i = 0; i < reducedPhasors.size(); ++i) {
    newPhasors[i]=reducedPhasors.get(i);
  }
  return newPhasors;
}
