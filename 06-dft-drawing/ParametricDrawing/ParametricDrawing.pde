void setup() {
    size(500,500);
    int N = 200;
    float tbpn = TWO_PI / N; 
    
    JSONArray points = new JSONArray();
    for (int i = 0; i < N; i++) {
        JSONObject point = new JSONObject();
        float theta = i*tbpn;
        float r=250*cos(cos(4*theta));
        point.setFloat("x",250+r*cos(theta));
        point.setFloat("y",250+r*sin(theta));
        points.setJSONObject(i,point);
    }

    saveJSONArray(points, "./cos_cos_4theta.json");
    noLoop();
    exit();
}

void draw() {
    
}
