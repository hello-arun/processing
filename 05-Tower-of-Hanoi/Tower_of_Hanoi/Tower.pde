class Tower{
    ArrayList<Integer> rings = new ArrayList();
    int w = 50,h = 20;
    String name;
    Tower(ArrayList rings,String name) {
        this.rings = rings;
        this.name = name;
    }
    
    void show() {
        fill(0,255,0);
        rectMode(CORNER);
        rect( -5,h / 2,10, -400);
        textSize(48);
        fill(0, 0,255);
        text(name, -20, h * 3); 
        int i = 0;
        for (int ring : rings) {
            rectMode(CENTER);
            fill(255, 100);
            rect(0, -i * h, w * ring, h);
            // textSize(24);
            // text("" + ring, - 25, - i * h);
            i++;
        }
    }
}



