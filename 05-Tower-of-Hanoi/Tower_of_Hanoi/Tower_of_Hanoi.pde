Tower A,B,C;
int i = 0;
void setup() {
    size(800,800);
    ArrayList<Integer> ringsA = new ArrayList();
    ArrayList<Integer> ringsB = new ArrayList();
    ArrayList<Integer> ringsC = new ArrayList();
    int[] _rings = new int[]{6,5,4,3,2,1};
    for (int ring : _rings) {
        ringsA.add(ring);
    } 
    noLoop();
    A = new Tower(ringsA,"A");
    B = new Tower(ringsB,"B");
    C = new Tower(ringsC,"C");
    draw_();
    T(A.rings.size(),A.rings,B.rings,C.rings);


}

void draw_() {
    pushMatrix();
    pushStyle();
    fill(255);
    rect(0,0,width,height);
    translate(width / 2, height * 3 / 4);
    translate( -width / 3,0);
    A.show();
    translate(width / 3,0);
    B.show();
    translate(width / 3,0);
    C.show();  
    save("./frames/" + nf(i,3) + ".png");
    i += 1;
    popStyle();
    popMatrix();
}

void T(int n, ArrayList<Integer> A,ArrayList<Integer> B,ArrayList<Integer> C) {
    if (n == 1)
    {
        int a = A.get(A.size() - 1);
        B.add(a);
        A.remove(A.size() - 1);
        draw_();
    }
    else {
        T(n - 1,A,C,B);
        int a = A.get(A.size() - 1);
        B.add(a);
        A.remove(A.size() - 1);
        draw_();
        T(n - 1,C,B,A);
    }
}
