# processing
Various processing art tutorials.

## How to setup

1. Download and install processing from https://processing.org/download.
2. Make sure the processing.exe folder is added to `system path`.
3. Always keep the foldername and main-file.pde of the same name.
4. To use the vs code for writing code. You need to install additional extensions.
5. To run the processing project execule
```
processing-java --sketch="complete-sketch-folder-path" --run
```
example
```
processing-java --sketch="c:\Users\jangi\Documents\GitHub\processing\templates\beesandbombs" --run
```

## Making gif/mp4 with frames
1. For impatiesnts : use this thats all
```
ffmpeg -framerate 50 -i ./frames/fr%03d.png -profile:v main -pix_fmt yuv420p  anim.mp4
```
This will make video using images having names fr001.png fr002.png ..... and so on. i.e. having 3 integers following "fr".
fps will control frame rate and yuv420p is I dont know.

2. If you want to have control over quality you can use 

```
ffmpeg -i ./frames/fr%03d.png -c:v libx264 -crf 8 -preset veryslow -c:a libmp3lame -b:a 320k anim.mp4
```
crf value determine how much quality we want. `crf 0` best quality `crf 18` visually losless.

3. You can also use `magick.exe` tool from imagemagic sw to make gifs. You can download it on https://legacy.imagemagick.org/script/download.php
```
magick -delay 2 -loop 0 ./frames/*.png anim.gif
```
FPS=100/delay. i.e. delay=2 means FPS=50.<br>
use -loop 1 to make non-looping gif.