int[] readPrimes(String fileName,int numPrimes) {
    int[] primes = new int[numPrimes];
    int k = 0;
    String[] lines = loadStrings(fileName);
    for (int i = 0; i < lines.length; i++) {
        String[] words = split(lines[i],' ');
        for (int j = 0; j < words.length; j++) {
            if (words[0].equals("#")) 
                break;
            else if (words[j].equals(""))
                continue;
            else if (k < numPrimes)
                primes[k++] = int(words[j]);
            else if (k ==  numPrimes)
                return primes;
        }
    }
    return primes;
}
int[] primes;
int h,v,pad;
float hgap,vgap;
color brown,blue,yellow,red;
int INDEX = 0;
int frames = 0;
void setup() {
    ellipseMode(CORNER);
    // rectMode(CENTER);
    brown = color(103,74,64);
    blue = color(80,163,164);
    yellow = color(252,175,56);
    red = color(249,83,53);
    h = 20;
    v = 20;
    size(1000, 1000);
    pad = 20;
    print(width + " " + height);
    hgap = (width - pad) / h;
    vgap = (height - pad) / v;
    pad = int(width - hgap * h);
    primes = readPrimes("primes.txt",h * v);
}

void draw() {
    if (frames < h * v)
    {   translate(pad / 2.0,pad / 2.0);
        background(50);
        INDEX = 0;
        drawDot(h / 2,v / 2,0);
        frames += 1;
        spiralStep(h / 2,v / 2,1,frames);
        saveFrame("./frames/fr###.png");
    }
    else if (frames < h * v + 25) {
        frames += 1;
        saveFrame("./frames/fr###.png");
    }
    else {
        exit();
    }
    
}


void spiralStep(int sx,int sy, int snum, int maxnum) {
    int hstep = 0;
    int vstep = 0;
    while(snum<maxnum)
    {   // up step
        for (int i = 0; i <= vstep; ++i) {
            drawLine(sx , sy,sx,sy - 1); 
            
            
            sy -= 1;
            drawDot(sx,sy,++snum);
            if (snum >=  maxnum)
                return;
        }
        vstep += 1;
        // left
        for (int i = 0; i <= hstep; ++i) {
            drawLine(sx , sy,sx - 1,sy); 
            
            sx -= 1;
            drawDot(sx,sy,++snum);
            if (snum >=  maxnum)
                return;
        }
        hstep += 1;
        // Down
        for (int i = 0; i <= vstep; ++i) {
            drawLine(sx , sy,sx,sy + 1); 
            
            sy += 1;
            drawDot(sx,sy,++snum);
            if (snum >=  maxnum)
                return;
        }
        vstep += 1;
        // Right
        for (int i = 0; i <= hstep; ++i) {
            drawLine(sx , sy,sx + 1,sy); 
            sx += 1;
            
            drawDot(sx,sy,++snum);
            if (snum >=  maxnum)
                return;
        }
        hstep += 1;
    }
} 

void drawLine(int sx,int sy,int sx_,int sy_) {
    stroke(yellow);
    pushMatrix();
    translate(hgap / 2,vgap / 2);
    line(sx * hgap, sy * vgap ,(sx_) * hgap,(sy_) * vgap); 
    popMatrix();
}
void drawDot(int sx,int sy,int num) {
    noStroke();
    if (num ==  0) {
        fill(red);
        float fact = 0.5;
        pushMatrix();
        translate(hgap / 2 - 0.5 * hgap * fact,vgap / 2 - 0.5 * vgap * fact);
        ellipse(sx * hgap, sy * vgap, hgap * fact, vgap * fact); 
        
        popMatrix();
        
    }
    else if (primes[INDEX] ==  num) {
        fill(yellow);
        INDEX++;
        ellipse(sx * hgap, sy * vgap, hgap, vgap); 
    }
    else{
        fill(blue);
        float  fact = 0.3;
        pushMatrix();
        translate(hgap / 2 - 0.5 * hgap * fact,vgap / 2 - 0.5 * vgap * fact);
        ellipse(sx * hgap, sy * vgap, hgap * fact, vgap * fact); 
        
        popMatrix();
        
    }
    
}