# 01-disclination

We will visualise the positive disclination(5-disclination) in graphene ring. Initially we have the preistine graphene flake.
![graphen.png](./graphene.png)

The red line denote the 60 deg area that we are going to cut from this ring. To maintain the minimum energy configuration the carbon atoms will adjust so as to not have any under or over coordination.


![graphen_final.png](./graphene_final.png)

Final Image

![graphen.png](./anim.gif)

Created with processing and magick
to create the animation first run the `v_01_disclination.pde` file and then

```
magick -loop 0 -delay 2 ./frames/*{1..49}.png -delay 100 ./frames/*50.png  ./anim.gif
```

I have used different delays because I want to stay at final image for some time.