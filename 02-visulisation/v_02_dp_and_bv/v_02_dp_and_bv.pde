PVector center,a; 
color[] palette = {#E7F2F8,#74BDCB,#FFA384,#EFE7BC};
void setup() {
    size(800,400);
    center = new PVector(0,0);
    a = new PVector(0.3*width,0.3*height);
    ellipseMode(CENTER);
    stroke(0);
}

void drawDislocation(PVector pos,float t1,float t2, color col){
    fill(col);
    ellipseMode(CENTER);
    arc(pos.x, pos.y, 1.8*height,1.8*height, t1,t2);
}
void draw() {
    a.x=mouseX;
    a.y=mouseY;
    /* Left figure */
    background(palette[0]);
    noFill();
    rect(0,0,0.5*width,height);
    clip(0,0,0.5*width,height);

    drawDislocation(center,0, PI/3,palette[1]);

    float theta = atan2(a.y,a.x);
    // println(a.y+" "+a.x+" "+theta);
    // drawDislocation(center,0,theta,color(255,0,0));
    drawDislocation(PVector.add(center,a),0, PI/3,palette[2]);
    pushMatrix();
    translate(center.x,center.y);
    line(0,0,a.x,a.y);
    popMatrix();

    noClip();
    noFill();

    rect(0.5*width,0,0.5*width,height);
    clip(0.5*width,0,0.5*width,height);
    translate(0.5*width,0);
    drawDislocation(center,0, PI/3,palette[1]);
    float x = a.mag();
    float y = 0;
    theta = PI/6;
    drawDislocation(new PVector(center.x+x,center.y+y),0, theta,palette[2]);
     x = a.mag()*cos(PI/3);
     y = a.mag()*sin(PI/3);

    drawDislocation(new PVector(center.x+x,center.y+y),theta, PI/3,palette[2]);
    pushMatrix();
    translate(x,y);

    rotate(theta);
    line(0,0,0,-a.mag());
    popMatrix(); // pushMatrix();
    // translate(center.x,center.y);
    // stroke(palette[3]);
    // float theta = atan2(a.y,a.x);
    // line(0,0,a.x*10,a.y*10);
    // popMatrix();

    // line(0,0,600,0);
    // line(0,0,width*cos(-PI/3),height*sin(-PI/3));

    // translate(a.x,a.y);
    // line(0,0,600,0);
    // line(0,0,width*cos(-PI/3),height*sin(-PI/3));
    // stop();
}
