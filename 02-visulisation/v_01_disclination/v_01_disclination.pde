Honeycomb honeycomb;
int numframes = 50;
PImage photo;
void setup() {
    size(600,600);
    noFill();
    honeycomb = new Honeycomb();
}
float t = 0;
void draw() {
    background(255);
    t = 1.0 * frameCount / numframes;
    if (frameCount==1){
        pushMatrix();
    honeycomb.show();
        popMatrix();
    strokeWeight(2);
    translate(width / 2, height / 2);
    stroke(255,0,0);
    point(0, 0);
    line(0, 0, 1000, 0);
    line(0, 0, 1000 * cos( -PI / 3), 1000 * sin( -PI / 3));
    stroke(255,0,0);
    translate(-3*cos(PI/6)*40, 40*3/2);
    line(0,0, 1000, 0);
    line(0,0, 1000 * cos( -PI / 3), 1000 * sin( -PI / 3));

    saveFrame("./graphene.png");
    photo = loadImage("./graphene.png");
    }
    loadPixels();    
    int d_theta = 4000;
    int dr = 400;
    for (int i = 0; i < d_theta; ++i) {
        for (int j = 0; j < dr; ++j) {
            float r = map(sqrt(1)*j,0,dr,0,400);
            float theta = map(i,0,d_theta,0,2 * PI - PI / 3);
            int x = width / 2 + (int)(r * cos(theta));
            int y = height / 2 + (int)(r * sin(theta));
            if (x >=  0 && x < 600 && y >=  0 && y < 600) {
                theta = map(theta,0,2 * PI - PI / 3,0,2 * PI- (1.0-t)*PI / 3);
                int x2 = width / 2 + (int)(r * cos(theta));
                int y2 = height / 2 + (int)(r * sin(theta));
                if (x2 >=  0 && x2 < 600 && y2 >=  0 && y2 < 600) {                
                    pixels[y2 * width + x2] = photo.pixels[y * width + x]; 
                }
            }
        } 
}
    updatePixels();
    if (frameCount <=  numframes) {
    println(frameCount);

        saveFrame("./frames/fr###.png");
    }
    else{
        stop();
        exit();
    }
    }
        