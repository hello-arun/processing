class Honeycomb{
    float cx = width/2,cy=height/2; // center_x center_y
    float bl = 40; //bondlength
    int nx = 5,ny=5; // no of horizontal and vertical units
    
    void show() {
        float  phase = PI / 6;
        stroke(0);    
        for (int k = -ny; k <= ny;k++) {
            for (int j = -nx; j <= nx; ++j) {
                pushMatrix();
                translate(cx + j * 2 * bl * cos(PI / 6),cy+3*bl*k);            
                beginShape();
                for (int i = 0; i < 6; ++i) {
                    float x = bl * cos(i * 2 * PI / 6 + phase);
                    float y = bl * sin(i * 2 * PI / 6 + phase);
                    vertex(x, y);            
                }
                endShape(CLOSE);        
                popMatrix();
            }
        } 

        for (int k = -ny; k <= ny;k++) {
            for (int j = -nx; j <= nx; ++j) {
                pushMatrix();
                translate(bl*cos(PI/6)+cx + j * 2 * bl * cos(PI / 6),cy+3.0*bl*k+1.5*bl);            
                beginShape();
                for (int i = 0; i < 6; ++i) {
                    float x = bl * cos(i * 2 * PI / 6 + phase);
                    float y = bl * sin(i * 2 * PI / 6 + phase);
                    vertex(x, y);            
                }
                endShape(CLOSE);        
                popMatrix();
            }
        }
    }
}