class MidPoints{
    Thing t1, t2;
    int numPoints;
     MidPoints(Thing t1,Thing t2,int numPoints){
        this.t1 = t1;
        this.t2 = t2;
        this.numPoints = numPoints;
    }
    
    void show() {
        pushStyle();
        strokeWeight(2);
        stroke(255,100);
        for (int i = 0;i <=  numPoints;i++) {
            float tt = 1.0 * i / numPoints;
            float x = lerp(t1.getX(t-tt),t2.getX(t-tt),tt);
            float y = lerp(t1.getY(t-tt),t2.getY(t-tt),tt);
            point(x,y);
        }
        popStyle();
    }
}
