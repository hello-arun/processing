class Thing{
    float cx,cy,r,size;
    int freq;
    Thing(float cx_,float cy_,float r_,int f_){
        cx=cx_;
        cy=cy_;
        r=r_;
        freq=f_;
        size = random(5,10);
    }
    float getX(float t){
        return cx+r*cos(2*PI*t*freq);
    }
    float getY(float t){    
        return cy+r*sin(2*PI*t*freq);
    }
    void show(){
        stroke(255);
        strokeWeight(size);
        point(getX(t),getY(t));
    }
}