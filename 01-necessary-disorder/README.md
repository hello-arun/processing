# Necessary Disorder Tutorials
These tutorials are taken from https://necessarydisorder.wordpress.com/2018/07/02/getting-started-with-making-processing-gifs-and-using-the-beesandbombs-template/ 

## [nd_01_looping_gif](./nd_01_looping_gif)
![animation.gif](./nd_01_looping_gif/anim.gif)

## [nd_02_looping_gif_blurr](./nd_02_looping_gif_blurr)
![animation.gif](./nd_02_looping_gif_blurr/anim.gif)

## [nd_03_example](./nd_03_example)
![animation.gif](./nd_03_example/anim.gif)

## [nd_04_example](./nd_04_example)
![animation.gif](./nd_04_example/anim.gif)

## [nd_05_example](./nd_05_example)
![animation.gif](./nd_05_example/anim.gif)

## [nd_06_ease_fun](./nd_06_ease_fun)
![animation.gif](./nd_06_ease_fun/anim.gif)

## [nd_07_noise_perlin](./nd_07_noise_perlin)
![animation.gif](./nd_07_noise_perlin/anim.gif)

## [nd_08_noise_open_simplex](./nd_08_noise_open_simplex)
![animation.gif](./nd_08_noise_open_simplex/anim.gif)

## [nd_09_big_example](./nd_09_big_example)
![animation.gif](./nd_09_big_example/anim.gif)

## [nd_10_lerp](./nd_10_lerp)
![animation.gif](./nd_10_lerp/anim.gif)

## [nd_11_replacement](./nd_11_replacement)
![animation.gif](./nd_11_replacement/anim.gif)